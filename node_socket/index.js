// https://medium.com/@jaouad_45834/basic-chat-web-app-using-express-js-vue-js-socket-io-429588e841f0

const express = require('express');


const app = express();



const server = app.listen(3000, function() {
    console.log('server running on port 3000');
});


const io = require('socket.io')(server);

var clients =[];
var currentUser=""
var listQuestions=null


io.on('connection', function(socket){

    console.log('a user connected');
    socket.join("waitingRoom")

    socket.on('monpseudo', (currentUser)=>{
      clients.push(currentUser);
    
      
    
      io.emit('listUsers',clients)
    })
  
    socket.on('invitation', (guest, index, currentUser)=>{
      socket.join(index)
      io.emit("askPlay",index, guest, currentUser)
      console.log("guest:",guest, "current:",currentUser, "server_guest:","room"+index)
  
    })
  
    socket.on("joinParty", (room, currentUser,host)=>{
      
      socket.join(room)
      
      var destination = '/quizz'
      io.to(room).emit('redirect', destination)
      clients.splice(clients.indexOf(currentUser),1)
      clients.splice(clients.indexOf(host),1)
      io.emit('listUsers',clients)
    })

    socket.on('listQuestions', (currentQuestions)=>{
             listQuestions=currentQuestions
        io.emit('receiveQuest', listQuestions)
      console.log("listQuestions:", listQuestions)
      
  
    socket.on('disconnect', function(){
        clients.splice(clients.indexOf(currentUser),1)
     
        io.emit('listUsers',clients)
        console.log("disconnected")
      });
  })
})