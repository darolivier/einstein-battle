from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login

from rest_framework import viewsets
from quizz.models import Level, Answer, Question, Thematic
from quizz.serializers import LevelSerializer, AnswerSerializer, QuestionSerializer, ThematicSerializer


# Create your views here.

class AnswerViewSet(viewsets.ModelViewSet):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer

class LevelViewSet(viewsets.ModelViewSet):
    queryset = Level.objects.all()
    serializer_class = LevelSerializer

class QuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer

class ThematicViewSet(viewsets.ModelViewSet):
    queryset = Thematic.objects.all()
    serializer_class = ThematicSerializer

