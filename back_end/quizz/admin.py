from django.contrib import admin
from quizz.models import Answer, Thematic, Level, Question

admin.site.register(Answer)
admin.site.register(Thematic)
admin.site.register(Level)
admin.site.register(Question)



# Register your models here.
