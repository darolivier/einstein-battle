from rest_framework import serializers
from quizz.models import Question, Answer, Thematic, Level



class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ['response', 'correct']

class LevelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Level
        fields = ['label']

class QuestionSerializer(serializers.ModelSerializer):
    answer_list = AnswerSerializer(many=True)

    class Meta:
        model = Question
        fields = ['statement', 'explication', 'level', 'answer_list']

class ThematicSerializer(serializers.ModelSerializer):
    question_list = QuestionSerializer(many=True)

    class Meta:
        model = Thematic
        fields = ['label', 'question_list']

