from django.contrib import admin
from django.urls import path, include
from quizz import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'thematic', views.ThematicViewSet)
router.register(r'answer', views.AnswerViewSet)
router.register(r'level', views.LevelViewSet)
router.register(r'question', views.QuestionViewSet)


urlpatterns = [
    path('api/', include(router.urls)),
]
