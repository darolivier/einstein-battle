from django.db import models

# Create your models here.


class Level(models.Model):
    label = models.TextField(default=None, blank=True, null=True)

    def __str__(self):
        return self.label

class Thematic(models.Model):
    label = models.TextField(default=None, blank=True, null=True)
    
    def __str__(self):
        return self.label

class Question(models.Model):
    statement = models.TextField(default=None, blank=True, null=True)
    explication = models.TextField(default=None, blank=True, null=True)
    level = models.ForeignKey(Level, related_name='level', on_delete = models.CASCADE)
    thematic = models.ForeignKey(Thematic, related_name='question_list', on_delete = models.CASCADE)

    class Meta:
        unique_together = ['statement']
        ordering = ['statement']

    def __str__(self):
        return self.statement

class Answer(models.Model):
    question = models.ForeignKey(Question, related_name='answer_list', on_delete = models.CASCADE)
    response = models.TextField(default=None, blank=True, null=True)
    correct = models.BooleanField(default=False)

    def __str__(self):
        return self.response

