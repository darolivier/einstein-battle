export default
{
  title: 'Hello',
  label: 'Maths',
  question_list: [
    {
      statement: 'Quel est ton nom ?',
      explication: 'Ton vrai nom ! Explication',
      level: 1,
      answer_list: [
        { response: 'Aurore', correct: true },
        { response: 'Jules' },
        { response: 'Olivier' },
        { response: 'Laurent' }
      ]
    },
    {
      statement: 'Quel est ta profession ?',
      explication: 'Ton vrai nom ! Explication',
      level: 1,
      answer_list: [
        {
          response: 'Dev',
          correct: true
        },
        {
          response: 'Boul',
          correct: false
        },
        {
          response: 'Elec',
          correct: false
        },
        {
          response: 'Pat',
          correct: false
        }
      ]
    }
  ]
}

/* {
  title: 'Quizz de Ipsum',

  questions: [
    {
      statement: 'Question 1',
      answer_list: [
        { response: 'Wrong, too bad.' },
        { response: 'Right!', correct: true }
      ]
    }, {
      statement: 'Question 2',
      answer_list: [
        { response: 'Right answer', correct: true },
        { response: 'Wrong answer' }
      ]
    }
  ]
} */
