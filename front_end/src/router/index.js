import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/quizz',
    name: 'quizz',
    component: () => import(/* webpackChunkName: "quizz" */ '../views/Quizz.vue')
  },
  {
    path: '/game',
    name: 'game',
    component: () => import(/* webpackChunkName: "quizz" */ '../views/RequestGame.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
