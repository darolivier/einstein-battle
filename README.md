# Einstein Battle

Quizz application for engineers, students and passionnated.
**Version 0.1.0-alpha**

# Install
Clone this repository.

### Back-end
Create your own virtual environment with python3 
```
python3 -m virtualenv
```
Enter inside your venv
```
source venv/bin/activate
```
Install dependencies
```
pip install -r requirements.txt
```


### Front-end
Be sure to have npm globally installed.
Install dependencies
```
npm install package.json
```


# Run your server
### Back-end
Don't forget to be in your venv then go to your project folder
```
cd backend
./manage.py runserver
```
### Front-end
```
cd frontend
npm run serve
```
